function problem4(inventory){
    let carsYearsList = []
    if (Array.isArray(inventory) && inventory.length > 0){
        for (let index = 0 ; index < inventory.length ; index++){  
            carsYearsList.push(inventory[index].car_year)
        }
        return carsYearsList    
    }
    return []
}
module.exports = problem4;
