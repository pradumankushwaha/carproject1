let inventory = require("../inventory")
const problem2 = require("../problem2")
const result = problem2(inventory);
if (result.constructor.name === "Object"){
    console.log(`Last car is a ${result.car_make} ${result.car_model}`)
}else{
    console.log("Please pass valid argument")
}
